// C'est l'heure bonus dans la grande ville ! Les gros chats se frottent les pattes par anticipation... mais qui va gagner le plus d'argent ?

// Construisez une fonction qui prend en compte deux arguments (salaire, bonus). Le salaire sera un entier et le bonus un booléen.

// Si le bonus est vrai, le salaire doit être multiplié par 10. Si le bonus est faux, le gros chat n'a pas gagné assez d'argent et doit recevoir uniquement son salaire déclaré.

// Renvoie le montant total que l'individu recevra sous forme de chaîne préfixée par "£" (= "\u00A3", JS, Go, Java, Scala et Julia), "$" (C#, C++, Ruby, Clojure, Elixir, PHP, Python, Haskell , et Lua) ou "¥" (Rouille).

function bonusTime(salary, bonus) {
    if (bonus) {
    salary *= 10;
  }
  
  return `£${salary}`;

}