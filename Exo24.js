// Étant donné le triangle des nombres impairs consécutifs :

//              1
//           3     5
//        7     9    11
//    13    15    17    19
// 21    23    25    27    29
// ...
// Calculez la somme des nombres de la n ème ligne de ce triangle (en commençant à l'index 1), par exemple : ( Entrée --> Sortie )

// 1 -->  1
// 2 --> 3 + 5 = 8


function rowSumOddNumbers(n) {
	return n*n*n
}