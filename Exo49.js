// Votre tâche consiste à ajouter une nouvelle propriété usersAnswerà chaque objet du tableau questions. La valeur de usersAnswerdoit être définie sur null. La solution devrait fonctionner pour un tableau de n'importe quelle longueur.

// Par exemple:

// var questions = [{
//     question: "What's the currency of the USA?",
//     choices: ["US dollar", "Ruble", "Horses", "Gold"],
//     corAnswer: 0
// }, {
//     question: "Where was the American Declaration of Independence signed?",
//     choices: ["Philadelphia", "At the bottom", "Frankie's Pub", "China"],
//     corAnswer: 0
// }];
// Après avoir ajouté la propriété, le résultat devrait être :

// var questions = [{
//     question: "What's the currency of the USA?",
//     choices: ["US dollar", "Ruble", "Horses", "Gold"],
//     corAnswer: 0,
//     usersAnswer: null
// }, {
//     question: "Where was the American Declaration of Independence signed?",
//     choices: ["Philadelphia", "At the bottom", "Frankie's pub", "China"],
//     corAnswer: 0,
//     usersAnswer: null
// }];
// Le questionstableau est déjà défini pour vous et n'est pas le même que celui de l'exemple.

questions.map((question)=> {
    question.usersAnswer = null;
});
