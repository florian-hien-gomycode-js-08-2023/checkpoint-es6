// Pouvez-vous trouver l'aiguille dans la botte de foin ?

// Écrivez une fonction findNeedle()qui prend arrayplein de déchets mais en contient un"needle"

// Une fois que votre fonction a trouvé l'aiguille, elle doit renvoyer un message (sous forme de chaîne) indiquant :

// "found the needle at position "en plus, indexil a trouvé l'aiguille, donc :

// Exemple (Entrée -> Sortie)

// ["hay", "junk", "hay", "hay", "moreJunk", "needle", "randomJunk"] --> "found the needle at position 5" 
// Remarque : En COBOL, il devrait renvoyer "found the needle at position 6"

function findNeedle(haystack) {
    let r = haystack.indexOf('needle')
    if(!r) return
    return `found the needle at position ${r}`
  }