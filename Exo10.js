
// Accueillir. Dans ce kata, il vous est demandé de mettre au carré chaque chiffre d'un nombre et de les concaténer.

// Par exemple, si nous exécutons 9119 via la fonction, 811181 apparaîtra, car 9 2 vaut 81 et 1 2 vaut 1. (81-1-1-81)

// Exemple n°2 : Une entrée de 765 retournera/devrait renvoyer 493625 car 7 2 vaut 49, 6 2 vaut 36 et 5 2 vaut 25. (49-36-25)

// Remarque : La fonction accepte un entier et renvoie un entier.

function squareDigits(num){
    return Number([...num.toString()].map(el => Math.pow(Number(el),2)).join(''))
}

