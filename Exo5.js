// Cette fois, pas d'histoire, pas de théorie. Les exemples ci-dessous vous montrent comment écrire une fonctionaccum :

// Exemples:
// accum("abcd") -> "A-Bb-Ccc-Dddd"
// accum("RqaEzty") -> "R-Qq-Aaa-Eeee-Zzzzz-Tttttt-Yyyyyyy"
// accum("cwAt") -> "C-Ww-Aaa-Tttt"
// Le paramètre de cumul est une chaîne qui comprend uniquement les lettres de a..zet A..Z.

function accum(s) {
    let result = ''
    s = Array.from(s)
    for (let index = 0; index < s.length; index++) {
        for (let i = -1; i < index; i++) 
            if(i<0) result += s[index].toUpperCase()
            else result += s[index].toLowerCase()
        result += '-'
    }
    return result.substring(0, result.length-1)
}