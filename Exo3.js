// On va vous donner un mot. Votre travail consiste à renvoyer le caractère central du mot. Si la longueur du mot est impaire, renvoie le caractère du milieu. Si la longueur du mot est paire, renvoyez les 2 caractères du milieu.

// #Exemples:

// Kata.getMiddle("test") should return "es"

// Kata.getMiddle("testing") should return "t"

// Kata.getMiddle("middle") should return "dd"

// Kata.getMiddle("A") should return "A"
// #Saisir

// Un mot (chaîne) de longueur 0 < str < 1000(en javascript, vous pouvez en obtenir un peu plus de 1000 dans certains cas de test en raison d'une erreur dans les cas de test). Vous n'avez pas besoin de tester cela. Ceci est uniquement là pour vous dire que vous n'avez pas à vous soucier de l'expiration du délai de votre solution.

// #Sortir

// Le(s) caractère(s) central(s) du mot représenté(s) sous forme de chaîne.

function getMiddle(s){
  return s.length%2===0 ? s[Math.floor(s.length/2)-1]+s[Math.floor(s.length/2)] : s[Math.floor(s.length/2)]
}