// Les trolls attaquent votre section de commentaires !

// Une manière courante de gérer cette situation consiste à supprimer toutes les voyelles des commentaires des trolls, neutralisant ainsi la menace.

// Votre tâche consiste à écrire une fonction qui prend une chaîne et renvoie une nouvelle chaîne avec toutes les voyelles supprimées.

// Par exemple, la chaîne « Ce site Web est destiné aux perdants MDR ! » deviendrait "Ths wbst s fr lsrs LL!".

// Remarque : car ce kata yn'est pas considéré comme une voyelle.



function disemvowel(str) {
    return [...str].filter(el => !['a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U'].includes(el)).join('');
  }