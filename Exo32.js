// Nathan adore le vélo.

// Parce que Nathan sait qu'il est important de rester hydraté, il boit 0,5 litre d'eau par heure de vélo.

// Vous obtenez le temps en heures et vous devez renvoyer le nombre de litres que Nathan boira, arrondi à la plus petite valeur.

// Par exemple:

// time = 3 ----> litres = 1

// time = 6.7---> litres = 3

// time = 11.8--> litres = 5

// https://www.codewars.com/kata/582cb0224e56e068d800003c/train/javascript

function litres(time) {
  return Math.floor(time/2);
  }