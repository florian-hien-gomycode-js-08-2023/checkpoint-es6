// Créez une fonction qui répond à la question « Jouez-vous au banjo ? ».
// Si votre nom commence par la lettre « R » ou un « r » minuscule, vous jouez du banjo !

// La fonction prend un nom comme seul argument et renvoie l'une des chaînes suivantes :

// name + " plays banjo" 
// name + " does not play banjo"
// Les noms donnés sont toujours des chaînes valides.

function areYouPlayingBanjo(name) {
    return name[0].toLowerCase()=='r' ? name + " plays banjo" : name + " does not play banjo";
  }