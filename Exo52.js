// Vous emmenez votre fils dans la forêt pour voir les singes. Vous savez qu'il y en a un certain nombre (n), mais votre fils est trop jeune pour apprécier le nombre complet, il doit commencer à les compter à partir de 1.

// En bon parent, vous vous asseoirez et compterez avec lui. Étant donné le nombre (n), remplissez un tableau avec tous les nombres jusqu'à ce nombre inclus, mais à l'exclusion de zéro.

// Par exemple (Entrée -> Sortie) :

// 10 --> [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
//  1 --> [1]

function monkeyCount(n) {
    let tab= []
    for(let i=0;i <n; i++){
      tab.push(i+1)
    }
    return tab
}