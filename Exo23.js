// Habituellement, lorsque vous achetez quelque chose, on vous demande si votre numéro de carte de crédit, votre numéro de téléphone ou votre réponse à votre question la plus secrète est toujours correct. Cependant, comme quelqu'un pourrait regarder par-dessus votre épaule, vous ne voulez pas que cela s'affiche sur votre écran. Au lieu de cela, nous le masquons.

// Votre tâche consiste à écrire une fonction maskifyqui transforme tous les caractères sauf les quatre derniers en '#'.

// Exemples (entrée --> sortie) :
// "4556364607935616" --> "############5616"
//      "64607935616" -->      "#######5616"
//                "1" -->                "1"
//                 "" -->                 ""

// // "What was the name of your first pet?"
// "Skippy" --> "##ippy"
// "Nananananananananananananananana Batman!" --> "####################################man!"



function maskify(cc) {
    return cc.length > 4? [...cc].fill('#', 0, cc.length-4).join(''): cc
}
