// Un isogramme est un mot qui n'a pas de lettres répétitives, consécutives ou non consécutives. Implémentez une fonction qui détermine si une chaîne contenant uniquement des lettres est un isogramme. Supposons que la chaîne vide soit un isogramme. Ignorez la casse des lettres.

// Exemple : (Entrée --> Sortie)

// "Dermatoglyphics" --> vrai "aba" --> faux "moOse" --> faux (ignorer la casse des lettres)

// isIsogram "Dermatoglyphics" = true
// isIsogram "moose" = false
// isIsogram "aba" = false

function isIsogram(str){
    return new Set(str.toLowerCase()).size === str.length
}5