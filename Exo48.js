// Vous ne pouvez même pas compter la quantité d'objets que votre client bâtard a déjà fabriqués. Il a beaucoup de chiens, et aucun d’entre eux ne le peut .bark().

// Pouvez-vous résoudre ce problème ou laisserez-vous ce client vous déjouer pour de bon ?

// Infos pratiques :
// La .bark()méthode d'un chien doit renvoyer la chaîne 'Woof!'.

// Le constructeur que vous avez créé (il est préchargé) ressemble à ceci :

// function Dog(name, breed, sex, age){
//     this.name  = name;
//     this.breed = breed;
//     this.sex   = sex;
//     this.age   = age;
// }
// Astuce : un de vos amis vient de vous expliquer comment Javascript gère les classes différemment des autres langages de programmation. Il ne pouvait pas s'empêcher de divaguer à propos de "prototypes" , ou quelque chose comme ça. Peut-être que ça pourrait t'aider...


function Dog(name, breed, sex, age){
    this.name  = name;
    this.breed = breed;
    this.sex   = sex;
    this.age   = age;
}

Dog.prototype.bark = function () {
    return 'Woof!';
};

const monChien = new Dog('Fido', 'Golden Retriever', 'Mâle', 3);

const criDuChien = monChien.bark();
