// Nous avons besoin d'une fonction simple qui détermine si un pluriel est nécessaire ou non. Il doit prendre un nombre et renvoyer vrai si un pluriel doit être utilisé avec ce nombre ou faux sinon. Cela serait utile lors de l'impression d'une chaîne telle que 5 minutes, 14 applesou 1 sun.

// Vous n'avez qu'à vous soucier des règles de grammaire anglaise pour ce kata, où tout ce qui n'est pas singulier (un de quelque chose) est au pluriel (pas un de quelque chose).

// Toutes les valeurs seront des entiers positifs ou des flottants, ou zéro.

function plural(n) {
    if (n == 1) return false;
    return true;
  }