// Considérez un tableau/une liste de moutons dont certains pourraient manquer chez eux. Nous avons besoin d'une fonction qui compte le nombre de moutons présents dans le tableau (vrai signifie présent).

// Par exemple,

// [true,  true,  true,  false,
//   true,  true,  true,  true ,
//   true,  false, true,  false,
//   true,  false, false, true ,
//   true,  true,  true,  true ,
//   false, false, true,  true]
// La bonne réponse serait 17.

// Astuce : N'oubliez pas de vérifier les mauvaises valeurs telles que null/undefined

function countSheeps(sheep) {
    return sheep.filter(el => el == true).length
  }