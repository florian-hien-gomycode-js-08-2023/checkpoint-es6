// Créez une classe Ball. Les objets Ball doivent accepter un argument pour le "type de balle" lorsqu'ils sont instanciés.

// Si aucun argument n’est donné, les objets balle doivent être instanciés avec un « type de balle » « régulier ».

// ball1 = new Ball();
// ball2 = new Ball("super");

// ball1.ballType     //=> "regular"
// ball2.ballType     //=> "super"

var Ball = function(ballType) {
    this.ballType = ballType || "regular"
  };