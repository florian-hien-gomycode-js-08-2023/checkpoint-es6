// Instructions
// Écrivez une fonction qui prend une seule chaîne ( word) comme argument. La fonction doit renvoyer une liste ordonnée contenant les index de toutes les lettres majuscules de la chaîne.

// Exemple (Entrée --> Sortie)
// "CodEWaRs" --> [0,3,4,6]



var capitals = function (word) {
    const upperCaseIndices = [];

for (let i = 0; i < word.length; i++) {
  if (word[i] === word[i].toUpperCase()) {
    upperCaseIndices.push(i);
  }
}

return upperCaseIndices;
};