// Vous connaissez peut-être des carrés parfaits assez grands. Mais qu’en est-il du SUIVANT ?

// Complétez la findNextSquareméthode qui trouve le carré parfait intégral suivant celui passé en paramètre. Rappelons qu'un carré parfait intégral est un entier n tel que sqrt(n) est également un entier.

// Si le paramètre lui-même n’est pas un carré parfait, il -1doit être renvoyé. Vous pouvez supposer que le paramètre est non négatif.

// Exemples :(Entrée --> Sortie)

// 121 --> 144
// 625 --> 676
// 114 --> -1 since 114 is not a perfect square


function findNextSquare(sq) {
    // Return the next square if sq is a perfect square, -1 otherwise
    return Number.isInteger(Math.sqrt(sq)) ? Math.pow((Math.sqrt(sq)+1),2) : -1;
  }