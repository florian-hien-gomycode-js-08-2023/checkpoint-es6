// Créez une fonction nommée divisors/ Divisorsqui prend un entier n > 1et renvoie un tableau avec tous les diviseurs de l'entier (sauf 1 et le nombre lui-même), du plus petit au plus grand. Si le nombre est premier, renvoie la chaîne '(entier) est premier' ( nullen C#, table vide en COBOL) (à utiliser Either String aen Haskell et Result<Vec<u32>, String>en Rust).

// Exemple:
// divisors(12); // should return [2,3,4,6]
// divisors(25); // should return [5]
// divisors(13); // should return "13 is prime"

function divisors(integer) {
    let tab=[]
      for(let i = 2; i < integer; i++) {
        if(integer % i == 0) {
            tab.push(i)
        }
    }
    return tab.length ==0 ? `${integer} is prime`: tab
  };