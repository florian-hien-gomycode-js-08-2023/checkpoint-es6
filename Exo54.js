// Switch/Case - Correction de bug n°6
// Oh non! La fonction evalObject de Timmy ne fonctionne pas. Il utilise Switch/Cases pour évaluer les propriétés données d'un objet, pouvez-vous corriger la fonction de Timmy ?



function evalObject(value){
    var result = 0;
    switch(value.operation){
      case'+': result = value.a + value.b;
        break;
      case'-': result = value.a - value.b;
        break;
      case'/': result = value.a / value.b;
        break;
      case'*': result = value.a * value.b;
        break;
      case'%': result = value.a % value.b;
        break;
      case'^': result = Math.pow(value.a, value.b);
        break;
    }
    return result;
  }